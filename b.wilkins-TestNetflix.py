#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10002:\n1450941\n1213181\n308502\n2581993\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10002:\n3.9\n3.6\n4.1\n3.8\n0.94\n")

    def test_eval_2(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.4\n3.4\n3.7\n0.91\n")

    def test_rms_less_than_1(self):
        r = StringIO("10008:\n1813636\n2048630\n930946\n1492860\n1687570\n1122917\n1885441\n")
        w = StringIO()
        netflix_eval(r,w)
        rsme = float(w.getvalue().strip().split()[-1])
        self.assertTrue(rsme < 1.00)

    def test_current_movie_variable(self):
        r = StringIO("10008:\n1813636\n2048630\n930946\n1492860\n1687570\n1122917\n1885441\n")
        for line in r.getvalue().strip().split():
            if line[-1] == ":":
                self.assertGreater(int(line[:-1]), 0)
                self.assertLess(int(line[:-1]), 17701)

    

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
