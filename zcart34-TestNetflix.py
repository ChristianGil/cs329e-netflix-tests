#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----


    def test_eval_1(self):
        r = StringIO("10022:\n510391\n268014\n2511719\n661571\n1824779\n2178013\n2104013\n1829777\n10023:\n1429287\n1321374\n2563028\n679315\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10022:\n2.9\n3.4\n3.2\n3.2\n3.5\n3.4\n3.0\n3.0\n10023:\n3.8\n3.7\n3.7\n3.6\n0.79\n")

    def test_eval_2(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.3\n3.3\n3.7\n0.89\n")

    def test_eval_3(self):
        r = StringIO("1:\n30878\n2647871\n1283744\n2488120\n317050\n1904905\n1989766\n14756\n1027056\n1149588\n1394012\n1406595\n2529547\n1682104\n2625019\n2603381\n1774623\n470861\n712610\n1772839\n1059319\n2380848\n548064\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.7\n3.5\n3.5\n4.2\n3.7\n3.8\n3.5\n3.7\n3.8\n3.6\n3.2\n3.6\n3.8\n3.8\n3.2\n3.9\n3.7\n4.0\n3.9\n3.9\n3.4\n4.3\n3.6\n0.78\n")



    # these tests aren't really needed. Just adding to remember to check for validity
    def test_rmse_value(self):
        r = StringIO("10021:\n2366406\n1033592\n249998\n1205724\n")
        w = StringIO()
        netflix_eval(r,w)
        w_list = w.getvalue().strip().split(sep="\n")
        rsme_to_check = float(w_list[-1])
        self.assertEqual(rsme_to_check,1.55)

    def test_valid_movie_id(self):
        r = StringIO("10028:\n1986255\n1312610\n1527666\n1606775\n1649392\n1522640\n440956\n10029:\n2293890\n1141552\n1823836\n2551226\n")
        r_list = r.getvalue().strip().split(sep="\n")
        for number in r_list:
            if number[-1] == ":":           # checks only the movie id
                movie_id_to_check = int(number[:-1])
                self.assertLessEqual(movie_id_to_check, 17770)
                self.assertGreater(movie_id_to_check,0)

    def test_valid_customer_id(self):
        r = StringIO("10028:\n1986255\n1312610\n1527666\n1606775\n1649392\n1522640\n440956\n")
        r_list = r.getvalue().strip().split(sep="\n")
        for number in r_list:
            if number[-1] != ":":               # excludes the movie id
                customer_id_to_check = int(number)
                self.assertLessEqual(customer_id_to_check, 2649429)
                self.assertGreater(customer_id_to_check, 0)

    def test_valid_rating(self):
        r = StringIO("10028:\n1986255\n1312610\n1527666\n1606775\n1649392\n1522640\n440956\n")
        w = StringIO()
        netflix_eval(r,w)
        w_list = w.getvalue().strip().split(sep="\n")
        for number in w_list[:-1]:
            if number[-1] != ":":               # excludes the movie id
                self.assertLessEqual(float(number),5.0)
                self.assertGreaterEqual(float(number),1.0)
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
