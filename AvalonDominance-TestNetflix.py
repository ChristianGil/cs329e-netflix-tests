#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    # tests if prediction is in [1.0,5.0]
    # and is rounded correctly
    def test_eval_prediction(self):
        r = StringIO("1:\n30878\n2647871\n1283744")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "1:\n3.8\n3.5\n3.4\n0.41")

    def test_eval_prediction_2(self):
        r = StringIO("10024:\n2059382\n1654205\n311377")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10024:\n3.5\n3.5\n3.6\n1.25"
        )

    # tests if rmse is rounded correctly
    def test_eval_rmse(self):
        r = StringIO("10027:\n401511\n2354884\n1951605")
        w = StringIO()
        netflix_eval(r, w)
        output = w.getvalue().strip()
        rmse = output.split('\n')[-1] # last line in output
        self.assertEqual(rmse, '0.77')

# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
