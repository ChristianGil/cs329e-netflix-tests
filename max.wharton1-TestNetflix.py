#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n2.9\n2.9\n4.3\n0.83\n")

    def test_eval_same(self): #same test case to guarantee consistent output
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n2.9\n2.9\n4.3\n0.83\n")

    def test_eval_invalid_movie(self): #test invalid movie input
        r = StringIO("Not a Number\n")
        w = StringIO()
        self.assertRaises(ValueError, netflix_eval, r, w)

    def test_eval_invalid_customer(self): #test invalid customer input
        r = StringIO("10040:\nNot a Number\n")
        w = StringIO()
        self.assertRaises(ValueError, netflix_eval, r, w)

    def test_eval_invalid_voth(self): #test invalid for both inputs
        r = StringIO("Not a Number\nNot a Number\n")
        w = StringIO()
        self.assertRaises(ValueError, netflix_eval, r, w)

# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.....
----------------------------------------------------------------------
Ran 5 tests in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          55      6      8      1    89%   23-24, 66-67, 94-95, 22->23
TestNetflix.py      30      0      0      0   100%
------------------------------------------------------------
TOTAL               85      6      8      1    92%

"""
