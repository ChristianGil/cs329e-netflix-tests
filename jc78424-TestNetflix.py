#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----


    def test_eval_all_of_customer_1379(self):
        r = StringIO("1379:\n2314792\n767665\n1642467\n87920\n")
        w = StringIO()
        netflix_eval(r, w)
        print("Movie 1379 check reviews of customers:\n"+w.getvalue())
        self.assertEqual(
            w.getvalue(), "1379:\n3.3\n3.3\n3.4\n3.8\n1.27\n")

    def test_eval_rmse_check(self):
        r = StringIO("1379:\n2314792\n767665\n1642467\n87920\n")
        w = StringIO()
        netflix_eval(r, w)
        rmse = w.getvalue().splitlines()[5]
        print("Movie 1379's prediction rmse:",rmse)
        if float(rmse) < 1:
            print("Passed! Your RMSE is smaller than 1.")
        else:
            print("Failed... Your RMSE is larger than 1.")


    def test_eval_two_movies(self):
        r = StringIO("10731:\n1879975\n953533\n10732:\n1888537\n2488901\n1283381\n469862\n88243")
        w = StringIO()
        print("Check rmse for two movies",w.getvalue())
        netflix_eval(r, w)
        print(w.getvalue())
        self.assertEqual(w.getvalue(), "10731:\n2.7\n4.0\n10732:\n4.5\n3.3\n3.9\n4.3\n4.2\n1.33\n")

'''
    def test_eval_1(self):
 		#with open('probe.txt', 'r') as myfile:
  		#	data = myfile.read()
 		#r = StringIO(probe.read())
        #r = StringIO(data)
        r = StringIO("2043:\n1417435\n2312054\n462685\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(   
            w.getvalue(), "2043:\n2.4\n2.4\n2.4\n1.27   \n")
'''
# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
